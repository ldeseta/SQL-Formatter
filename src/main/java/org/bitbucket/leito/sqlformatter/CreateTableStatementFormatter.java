/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package org.bitbucket.leito.sqlformatter;

import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.table.Index;

/**
 * Class that formats a single create table statement.
 */
public class CreateTableStatementFormatter {

    static String FOUR_SPACES_TAB = "    ";

    /**
     * Formats this create table statement.
     *
     * @param statement Original create table statement
     * @return Formatted statement
     */
    public static String format(CreateTable statement) {
        StringBuilder output = new StringBuilder();
        output
                .append("CREATE TABLE ")
                .append(statement.getTable())
                .append(" (\n");
        for (ColumnDefinition column : statement.getColumnDefinitions()) {
            output
                    .append(FOUR_SPACES_TAB)
                    .append(column)
                    .append(",\n");
        }
        if (statement.getIndexes() != null) {
            for (Index index : statement.getIndexes()) {
                output
                        .append(FOUR_SPACES_TAB)
                        .append(index)
                        .append(",\n");
            }
        }
        output.deleteCharAt(output.lastIndexOf(","));
        output.append(");\n");
        return output.toString();
    }
}
