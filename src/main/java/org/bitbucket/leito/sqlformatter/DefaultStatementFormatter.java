/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package org.bitbucket.leito.sqlformatter;

import net.sf.jsqlparser.statement.Statement;

/**
 * Class that formats a single statement with default toString representation.
 */
public class DefaultStatementFormatter {

    /**
     * Formats this statement.
     * 
     * @param statement Original statement
     * @return Formatted statement
     */
    public static String format(Statement statement) {
        return statement + ";\n\n";
    }
}
