DROP TABLE genre;

CREATE TABLE genre (
    id bigint primary key,
    description varchar (255) not null,
    enabled boolean
);

CREATE INDEX index_name ON movie (title, release_date);

DROP TABLE movie IF EXISTS;

-- -----------------------------------------------------------------------------
-- movie
-- -----------------------------------------------------------------------------
INSERT INTO movie
(id  , title                 , release_date, enabled, id_genre) VALUES
(3   , 'Grand Hotel Budapest', '2011-11-04', false  , 5       );
